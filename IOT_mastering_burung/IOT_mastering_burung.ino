//blynk AUTH
#define BLYNK_TEMPLATE_ID "TMPLrfjXb0w0"
#define BLYNK_DEVICE_NAME "PATIS LATICAU"
#define BLYNK_AUTH_TOKEN "8jqdsK8QNB2MBUZdeswcUpkhghxObAin"
#define BLYNK_FIRMWARE_VERSION "0.4.0"

#define pin_servo D5     //pin PWM
#define pin_motor_dc D6  //pin PWM
#define RX D1
#define TX D2

// NTP server tanggal&jam
#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Servo.h>
#include <EEPROM.h>

#include <DFPlayerMini_Fast.h>

#include <SoftwareSerial.h>
SoftwareSerial mySerial(RX, TX);  // RX, TX

DFPlayerMini_Fast myMP3;


Servo myservo;

#define BLYNK_PRINT Serial
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

const long utcOffsetInSeconds = 25200;
char daysOfTheWeek[7][12] = { "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu" };
byte da;
byte getday;
byte alarmd;
int menit;
byte change;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "id.pool.ntp.org", utcOffsetInSeconds);

bool stopnotifi;
int address = 0;
long ival[10];
long buff_ival[10];
long flag_alarm;
long monitoring;
byte lock = 0;
String buff[5];

bool led_set[2];
long timer_start_set[2] = { 0xFFFF, 0xFFFF };
long timer_stop_set[2] = { 0xFFFF, 0xFFFF };
long NTP_epoc[2] = { 0xFFFF, 0xFFFF };

char auth[] = BLYNK_AUTH_TOKEN;

char ssid[] = "Lt 4";
char pass[] = "Mess9876";

//long Timer MASTERING;

BLYNK_WRITE(V2) {
  unsigned char week_day;

  TimeInputParam t(param);

  if (t.hasStartTime() && t.hasStopTime()) {
    timer_start_set[0] = (t.getStartHour() * 60 * 60) + (t.getStartMinute() * 60) + t.getStartSecond();  //get epoc
    timer_stop_set[0] = (t.getStopHour() * 60 * 60) + (t.getStopMinute() * 60) + t.getStopSecond();      //get epoc

    Serial.print(String("Start Time: ") + t.getStartHour() + ":" + t.getStartMinute() + ":" + t.getStartSecond());

    Serial.println(timer_start_set[0]);
    buff[0] = (String("Start Time: ") + t.getStartHour() + ":" + t.getStartMinute() + ":" + t.getStartSecond());

    da = getday + alarmd;
    if (da > 7) { da = da - 7; }

    Serial.print(String("Stop Time: ") + t.getStopHour() + ":" + t.getStopMinute() + ":" + t.getStopSecond());
    Serial.println(timer_stop_set[0]);
    buff[1] = (String("Stop Time: ") + t.getStopHour() + ":" + t.getStopMinute() + ":" + t.getStopSecond());

  } else {
    timer_start_set[0] = 0xFFFF;
    timer_stop_set[0] = 0xFFFF;
  }
}

//long Timer Makan;
BLYNK_WRITE(V3) {
  unsigned char week_day;

  TimeInputParam t(param);

  if (t.hasStartTime()) {
    timer_start_set[1] = (t.getStartHour() * 60 * 60) + (t.getStartMinute() * 60) + t.getStartSecond();  //get epoc

    Serial.print(String("Start Time: ") + t.getStartHour() + ":" + t.getStartMinute() + ":" + t.getStartSecond());

    Serial.println(timer_start_set[1]);
    buff[2] = (String("Start Time: ") + t.getStartHour() + ":" + t.getStartMinute() + ":" + t.getStartSecond());
  } else {
    timer_start_set[1] = 0xFFFF;
  }
}
BLYNK_WRITE(V4)  // tombol +
{
  int pinValue = param.asInt();  // assigning incoming value from pin

  if (pinValue == 1) {
    alarmd = alarmd + 1;
  }
  Serial.println(" + ");
}

BLYNK_WRITE(V5)  // tombol -
{
  int pinValue = param.asInt();  // assigning incoming value from pin

  if (pinValue == 1) {
    alarmd = alarmd - 1;
    if (alarmd < 0) { alarmd = 0; }
  }
  Serial.println(" - ");
}
void setup() {

  Serial.begin(115200);
  pinMode(pin_motor_dc, OUTPUT);
  Serial.println("");
  Serial.println("Servo INIT POS 90");
  myservo.attach(pin_servo);
  myservo.write(90);
  delay(1000);  // servo atur posisi awal
  EEPROM.begin(512);
  Serial.println("");
  Serial.println("READ EEPROM......");
  baca_eeprom();

  WiFi.mode(WIFI_STA);
  WiFi.persistent(false);
  WiFi.disconnect(true);

  WiFi.begin(ssid, pass);
  Serial.println("");
  Serial.println("Try Connect WIFI");

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Koneksi WIFI ok");
  Blynk.begin(auth, ssid, pass);
  Serial.println("");
  Serial.println("Koneksi BLYNK ok");
  timeClient.begin();
  Serial.println("");
  Serial.println("NTP START");
}

void loop() {
  Alarm();
  Blynk.run();
  delay(500);
}


//===================================== fungsi fungsi=========================================================
long EEPROMReadlong(long address) {
  long four = EEPROM.read(address);
  delay(20);
  long three = EEPROM.read(address + 1);
  delay(20);
  long two = EEPROM.read(address + 2);
  delay(20);
  long one = EEPROM.read(address + 3);
  delay(20);
  Serial.print("READ address ");
  Serial.print(address);
  Serial.print(" = ");
  Serial.println(((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF));
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}

void EEPROMWritelong(int address, long value) {

  byte four = (value & 0xFF);
  byte three = ((value >> 8) & 0xFFFF);
  byte two = ((value >> 16) & 0xFFFFFF);
  byte one = ((value >> 24) & 0xFFFFFFFF);

  EEPROM.write(address, four);
  delay(20);
  EEPROM.write(address + 1, three);
  delay(20);
  EEPROM.write(address + 2, two);
  delay(20);
  EEPROM.write(address + 3, one);
  delay(20);
  Serial.println("WRITE EEPROM");
  if (EEPROM.commit()) {
    Serial.println("EEPROM successfully committed");
    delay(100);
  } else {
    Serial.println("ERROR! EEPROM commit failed");
    delay(20);
  }
  //EEPROMReadlong(address);
}
String date_parshing(long a_date) {
  long a = a_date / 60;
  long b = a_date % 60;  //sec
  long c = a / 60;       //hour
  long d = a % 60;       //minute


  return String(String(c) + ":" + String(d) + ":" + String(b));
}
void dummy_eeprom() {
  for (int i = 0; i <= 7; i++) {
    EEPROMWritelong(i * 10, 0);
    Serial.print("Simpan ival ");
    Serial.println(i);
  }
  baca_data();
}
void baca_data() {
  timer_start_set[0] = buff_ival[0];
  timer_stop_set[0] = buff_ival[1];
  timer_start_set[1] = buff_ival[2];
  flag_alarm = buff_ival[3];
  monitoring = buff_ival[4];
  da = buff_ival[5];
  alarmd = buff_ival[6];
  stopnotifi = buff_ival[7];
}

void refresh_data() {
  buff_ival[0] = timer_start_set[0];
  buff_ival[1] = timer_stop_set[0];
  buff_ival[2] = timer_start_set[1];
  buff_ival[3] = flag_alarm;
  buff_ival[4] = monitoring;
  buff_ival[5] = da;
  buff_ival[6] = alarmd;
  buff_ival[7] = stopnotifi;
}

void to_blynk() {
  Serial.print("lama mastering : ");
  Serial.print(alarmd);
  Serial.println(" hari");
  Blynk.virtualWrite(V11, alarmd);
  if (change < 5) {
    Blynk.virtualWrite(V9, "Start : " + date_parshing(timer_start_set[0]));
    Blynk.virtualWrite(V10, "Stop  : " + date_parshing(timer_stop_set[0]));
    change++;
  } else if (change < 10) {
    Blynk.virtualWrite(V9, " Makan & minum   ");
    Blynk.virtualWrite(V10, "Start : " + date_parshing(timer_start_set[1]));
    change++;
  } else if (change == 10) {
    change = 0;
  }
  Blynk.virtualWrite(V12, "Hari  : " + String(daysOfTheWeek[timeClient.getDay()]));
  Blynk.virtualWrite(V13, "Waktu : " + String(timeClient.getHours()) + ":" + String(timeClient.getMinutes()) + ":" + String(timeClient.getSeconds()));
  Blynk.virtualWrite(V7, flag_alarm);
}

void baca_eeprom() {
  ival[0] = EEPROMReadlong(0);
  ival[1] = EEPROMReadlong(10);
  ival[2] = EEPROMReadlong(20);
  ival[3] = EEPROMReadlong(30);
  ival[4] = EEPROMReadlong(40);
  ival[5] = EEPROMReadlong(50);
  ival[6] = EEPROMReadlong(60);
  ival[7] = EEPROMReadlong(70);


  buff_ival[0] = ival[0];
  buff_ival[1] = ival[1];
  buff_ival[2] = ival[2];
  buff_ival[3] = ival[3];
  buff_ival[4] = ival[4];
  buff_ival[5] = ival[5];
  buff_ival[6] = ival[6];
  buff_ival[7] = ival[7];

  baca_data();
}

void EEPROM_saveCek() {
  refresh_data();
  for (int i = 0; i <= 7; i++) {
    if (buff_ival[i] != ival[i]) {
      ival[i] = buff_ival[i];
      EEPROMWritelong(i * 10, buff_ival[i]);
      Serial.print("Simpan ival ");
      Serial.print(i);
      Serial.print(" = ");
      Serial.println(ival[i]);
    }
  }
}
void minum() {
  Serial.println("Minum");
  digitalWrite(pin_motor_dc, HIGH);
  delay(2000);
  digitalWrite(pin_motor_dc, LOW);
}
void makan() {
  while (1) {
    int pos;
    for (pos = 90; pos >= 0; pos -= 4) {  // goes from 90 degrees to 0 degrees
      myservo.write(pos);                 // tell servo to go to position in variable 'pos'
      delay(10);                          // waits 10ms for the servo to reach the position
      Serial.print(pos);
      Serial.println(" tutup");
    }

    for (pos = 0; pos <= 90; pos += 4) {  // goes from 0 degrees to 90 degrees
      myservo.write(pos);                 // tell servo to go to position in variable 'pos'
      delay(10);                          // waits 10ms for the servo to reach the position
      Serial.print(pos);
      Serial.println(" buka");
    }
    Serial.println("break_servo");
    break;
  }
}
void NTP_cek() {
  timeClient.update();
  NTP_epoc[0] = (timeClient.getHours() * 60 * 60) + (timeClient.getMinutes() * 60) + timeClient.getSeconds();
  Serial.println("==================================================================================================================");
  Serial.print(daysOfTheWeek[timeClient.getDay()]);
  Serial.print(", ");
  Serial.print(timeClient.getHours());
  Serial.print(":");
  Serial.print(timeClient.getMinutes());
  Serial.print(":");
  Serial.print(timeClient.getSeconds());
  Serial.print(". Epoc = ");
  Serial.print(NTP_epoc[0]);
  Serial.print(", Mastering Start = ");
  Serial.print(buff[0]);
  Serial.print(", Stop = ");
  Serial.print(buff[1]);
  Serial.print(", Makan & Minum = ");
  Serial.println(buff[2]);
  Serial.println("====================================================================================================================");
  Serial.println("");
  getday = timeClient.getDay();
  menit = timeClient.getMinutes();
}


void Alarm() {

  EEPROM_saveCek();  //looping auto save eeprom
  NTP_cek();         //cek tgl via online


  if (timer_start_set[0] == NTP_epoc[0]) {
    flag_alarm = 1;
    Serial.println("flag  ON");
    lock = 0;
  }  // cek flag
  if (timer_stop_set[0] == NTP_epoc[0]) {
    flag_alarm = 0;
    Serial.println("flag  OFF");
    lock = 0;
  }  // cek flag


  if (flag_alarm == 1 && lock == 0)  // mastering suara
  {
    Blynk.notify("Mastering start");
    Serial.println("wakeup DF player");
    // myMP3.wakeUp();
    delay(1000);
    lock = 1;

    Serial.println("Setting volume to max");
     myMP3.volume(30);

    Serial.println("Looping track ");
    myMP3.startRepeatPlay ();
  } else if (flag_alarm == 0 && lock == 0) {
    //Blynk.notify("Mastering Stop");
    Serial.println("Setting volume to min");
     myMP3.volume(1);

    Serial.println("Looping track ");
    myMP3.stopRepeat ();

    Serial.println("Sleep DF player");
    // myMP3.sleep ();
    delay(1000);
    lock = 1;
  }


  if (timer_start_set[1] == NTP_epoc[0])  // makan minum
  {
    Blynk.notify("Burung dikasih Makan");
    makan();
    delay(200);
    minum();
  }


  if (menit == 30 || menit == 59)  //notifikasi mastering harian
  {
    if (getday == da && stopnotifi == 0) {
      Blynk.notify("CEK Mastering");
      stopnotifi = 1;
    }
  } else if (menit == 29 || menit == 58) {
    stopnotifi = 0;
  }

  to_blynk();
}

//============================================ end fungsi fungsi ===============================================
