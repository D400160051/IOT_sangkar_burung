 

/*
    Program Memutar MP3 Player dengan Blynk
    dibuat oleh Indobot
*/

//Pemanggilan Library untuk wifi, blynk, dan DFPlayer Mini
#include <ESP8266WiFi.h>;
#include <BlynkSimpleEsp8266.h>;
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
#define BLYNK_PRINT Serial  
 
char auth[] = "************"; // masukkan kode autentikasi disini
char ssid[] = "************"; //nama wifi
char pass[] = "************"; //password

SoftwareSerial mySoftwareSerial(D1, D2); //Pin D1 dan D2 untuk DFPlayer
DFRobotDFPlayerMini myDFPlayer;

int sliderVolume = 0; //Slider untuk volume
int playNumber = 1; //Player ke
int maksList = 10; //maksimal list di microSD
boolean isPlaying = false;


BLYNK_WRITE(V4)
{
  String action = param.asStr();

  if (action == "play") {   //ketika kita menekan play
    myDFPlayer.play(playNumber); //Memainkan list pertama
    isPlaying = true;
    Serial.println("Playing..");
  }
  if (action == "stop") {   //ketika kita menekan stop
    myDFPlayer.pause();
    isPlaying = false;
    Serial.println("Paused..");
  }
  if (action == "next") {   //ketika kita menekan next
    if (isPlaying) {
    myDFPlayer.next();
    if (playNumber <= maksList)  playNumber++;
    Serial.println("Next Song..");
  }
  }
  if (action == "prev") {   //ketika kita menekan previous
    if (isPlaying) {
    myDFPlayer.previous();
    if (playNumber > 0)  playNumber--;
    Serial.println("Previous Song..");
  }
  }

  Blynk.setProperty(V4, "label", action);
  Serial.print(action);
  Serial.println();
}

BLYNK_WRITE(V5){
    sliderVolume = param.asInt(); //Pengaturan Volume maks 30
    myDFPlayer.volume(sliderVolume);
}


 
void setup(){  //Pengaturan Pin dan Variabel
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
  mySoftwareSerial.begin(9600);

    Serial.println();
    Serial.println("DFPlayer Mini Demo");
    Serial.println("Initializing DFPlayer...");

    if (!myDFPlayer.begin(mySoftwareSerial)) {
        Serial.println("Mulai DFPlayer Mini");
        while (true);
    }
    Serial.println(F("DFPlayer Mini Siap"));
    myDFPlayer.setTimeOut(500);
    
    //----Set different EQ----
    myDFPlayer.EQ(DFPLAYER_EQ_NORMAL);
    //  myDFPlayer.EQ(DFPLAYER_EQ_POP);
    //  myDFPlayer.EQ(DFPLAYER_EQ_ROCK);
    //  myDFPlayer.EQ(DFPLAYER_EQ_JAZZ);
    //  myDFPlayer.EQ(DFPLAYER_EQ_CLASSIC);
    //  myDFPlayer.EQ(DFPLAYER_EQ_BASS);
    myDFPlayer.outputDevice(DFPLAYER_DEVICE_SD);
}


void loop(){  //Perulangan Program
  Blynk.run();
}
